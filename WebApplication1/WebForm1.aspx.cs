using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        //You can get a valid token at https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insert your JWT token INSERT_VALID_ACCESS_TOKEN
        static string INSERT_VALID_ACCESS_TOKEN = "TokenDeAutenticacao";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CarimbarDocumento_Click(object sender, EventArgs e)
        {
            try
            {

                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                //Chamada da função que manda a requisição para o serviço de carimbo
                string responseInitialize = CarimbarDocumento().Result;
                ResponseTimeStamp responseTs = new ResponseTimeStamp();

                //Desserialização
                responseTs = Deserialize<ResponseTimeStamp>(responseInitialize);

                string path = AppDomain.CurrentDomain.BaseDirectory;
                string fileName = "carimboDoTempo-1.tst";

                //Pegando o valor em Base64 que contém o Carimbo, e convertendo de volta para Byte Array
                byte[] bytes = Convert.FromBase64String(responseTs.timeStamps[0].content);

                //Escrevendo Byte Array contendo o Carimbo em arquivo
                File.WriteAllBytes(path + fileName, bytes);

                if (responseInitialize == null)
                throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                // O resultado do BRy Framework é uma string com o JSON contendo os carimbos
                this.TextBoxSaidaInicializar.Text = "Carimbo do Tempo gerado com sucesso no diretório " + path + fileName + ".";

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }


        public async static Task<string> CarimbarDocumento()
        {
            var requestContent = new MultipartFormDataContent();

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./documento.pdf", FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            // Calculando o HASH para envio na Requisição - Melhor performance em relação a enviar o documento inteiro
            byte[] hashBytes;

            using (fileStream)
            {
                System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256.Create();
                hashBytes = sha256.ComputeHash(fileStream);
            }

            string hashString = BitConverter.ToString(hashBytes).Replace("-", String.Empty);

            requestContent.Add(new StringContent("123"), "nonce"); // Nonce da Requisição
            requestContent.Add(new StringContent("HASH"), "format"); //FILE, HASH ou REQUEST
            requestContent.Add(new StringContent("SHA256"), "hashAlgorithm"); // Algoritmo de hash

            requestContent.Add(new StringContent("1"), "documents[0][nonce]"); // Nonce do documento
            requestContent.Add(new StringContent(hashString), "documents[0][content]"); // Documento 1 - Neste exemplo é o HASH de um documento PDF

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            //Envia os dados para emissão do Carimbo do Tempo em formato Multipart
            var response = await client.PostAsync("https://fw2.bry.com.br/api/carimbo-service/v1/timestamps", requestContent).ConfigureAwait(false);

            // O resultado do BRy Framework é uma string com o JSON contendo os carimbos
            string resposta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return resposta;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }

    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    public class ResponseTimeStamp
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public List<TimeStamp> timeStamps;

        public ResponseTimeStamp()
        {
            this.timeStamps = new List<TimeStamp>();
        }
    }

    [DataContract]
    public class TimeStamp
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public string content;

    }


}
