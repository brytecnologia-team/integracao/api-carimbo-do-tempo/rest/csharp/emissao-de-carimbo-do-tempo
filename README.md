# Emissão de Carimbo do Tempo

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia CSharp para emissão de carimbo do tempo. 

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.


**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à emissão do carimbo do tempo.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| ACCESS_TOKEN | Access Token para o consumo do serviço (JWT). | WebForm1.aspx.cs


### Uso

- Passo 1: Execute o Build da aplicação utilizando o Microsoft Visual Studio.
- Passo 2: Certifique-se de que possui a BRy Extension instalada em seu navegador.
- Passo 3: Execute o exemplo.
